import DAO.UsersDAO;
import connection.CustomConnection;
import connection.MySqlConnector;
import entity.Users;
import lombok.extern.log4j.Log4j;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

@Log4j
public class Tests {
    private UsersDAO usersDAO;


    public Tests() {
        this.usersDAO = new UsersDAO();
    }


    @Test
    public void shouldConnectToDatabase() throws Exception {
        CustomConnection mySqlConnection = new MySqlConnector();
        Connection connection = mySqlConnection.getConnection();

        Assert.assertNotNull(connection);
        Assert.assertNotNull(connection.getMetaData().getURL());
    }

    @Test
    public void shouldFindUserByName() {
        Users result = usersDAO.showUserData("admin");
        System.out.println(result.toString());
        System.out.println(result.getUserpassword());
        Assert.assertNotNull(result);

    }
}