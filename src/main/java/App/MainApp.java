package App;

import menu.MenuFuncts;

public class MainApp {

    public MainApp() {
    }

    public static void main(String[] args) {


        MenuFuncts menu = new MenuFuncts();


        int x = menu.startMenu();
        if (x > 0) {
            menu.adminMenu();
        } else {
            menu.userMenu(x);
        }
    }
}