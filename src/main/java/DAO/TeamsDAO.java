package DAO;

import connection.CustomConnection;
import connection.MySqlConnector;
import entity.Countries;
import entity.Teams;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TeamsDAO {

    private CustomConnection connector;

    public TeamsDAO() {
        connector = new MySqlConnector();
    }

    public List<Teams> findAllTeams() {
        List<Teams> teamsList = new ArrayList<>();
        String query = "SELECT * FROM teams JOIN countries USING (country_id)";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Teams team = new Teams();
                team.setTeamId(resultSet.getInt("team_id"));
                team.setTeamName(resultSet.getString("team_name"));
                Countries countries = new Countries();
                countries.setCrid(resultSet.getInt("country_id"));
                countries.setCrname(resultSet.getString("country_name"));
                team.setCountries(countries);
                teamsList.add(team);
            }
            return teamsList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }
}
