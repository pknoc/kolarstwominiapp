package DAO;

import connection.CustomConnection;
import connection.MySqlConnector;
import entity.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultsDAO {

    private CustomConnection connector;

    public ResultsDAO() {
        connector = new MySqlConnector();
    }

    public List<Results> findResultByRace(int raceID) {
        List<Results> resultsList = new ArrayList<>();
        String query = "SELECT * FROM results \n" +
                "JOIN riders USING (rider_id) \n" +
                "JOIN countries USING (country_id) \n" +
                "JOIN teams USING (team_id) \n" +
                "JOIN races USING (race_id) \n" +
                "JOIN classifications USING (classification_id) \n" +
                "JOIN places USING(place_id)\n" +
                "WHERE race_id = " + raceID;

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Results results = new Results();

                Places place = new Places();
                place.setPlaceid(resultSet.getInt("place_id"));
                place.setPlaceName(resultSet.getString("place_name"));
                results.setPlace(place);

                Riders rider = new Riders();
                rider.setRiderId(resultSet.getInt("rider_id"));
                rider.setName(resultSet.getString("name"));
                rider.setSurname(resultSet.getString("surname"));

                Teams team = new Teams();
                team.setTeamId(resultSet.getInt("team_id"));
                team.setTeamName(resultSet.getString("team_name"));

                Countries countries = new Countries();
                countries.setCrid(resultSet.getInt("country_id"));
                countries.setCrname(resultSet.getString("country_name"));
                team.setCountries(countries);
                rider.setTeams(team);

                Countries country = new Countries();
                country.setCrid(resultSet.getInt("country_id"));
                country.setCrname(resultSet.getString("country_name"));
                rider.setCountries(country);

                results.setRiders(rider);

                Races race = new Races();
                race.setRaceId(resultSet.getInt("race_id"));
                race.setRaceName(resultSet.getString("race_name"));

                Classifications classification = new Classifications();
                classification.setCaId(resultSet.getInt("classification_id"));
                classification.setCaName(resultSet.getString("classification_name"));

                race.setClassifications(classification);

                Countries countriesRace = new Countries();
                countriesRace.setCrid(resultSet.getInt("country_id"));
                countriesRace.setCrname(resultSet.getString("country_name"));
                race.setCountries(country);

                results.setRaces(race);

                resultsList.add(results);

            }
            Collections.sort(resultsList);
            return resultsList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public void addResults(Results newResult) {
        String query = "INSERT INTO results (rider_id, race_id, place_id) VALUES (" +
                newResult.getRider_id() + ", " + newResult.getRace_id() + ", " + newResult.getPlace_id() + ")";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteResults(int raceId) {
        String query = "DELETE FROM results WHERE race_id = " + raceId;

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
