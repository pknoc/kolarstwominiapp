package DAO;

import connection.CustomConnection;
import connection.MySqlConnector;
import entity.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RacesDAO {

    private CustomConnection connector;

    public RacesDAO() {
        connector = new MySqlConnector();
    }

    public List<Races> findAllRaces() {
        List<Races> racesList = new ArrayList<>();
        String query = "SELECT * FROM races JOIN countries USING (country_id) JOIN classifications USING (classification_id)";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Races race = new Races();
                race.setRaceId(resultSet.getInt("race_id"));
                race.setRaceName(resultSet.getString("race_name"));

                Classifications classification = new Classifications();
                classification.setCaId(resultSet.getInt("classification_id"));
                classification.setCaName(resultSet.getString("classification_name"));
                race.setClassifications(classification);

                Countries country = new Countries();
                country.setCrid(resultSet.getInt("country_id"));
                country.setCrname(resultSet.getString("country_name"));
                race.setCountries(country);

                racesList.add(race);
            }
            return racesList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public List<Races> findRacesByClassification(int caId) {
        List<Races> racesList = new ArrayList<>();
        String query = "SELECT * FROM races JOIN countries USING (country_id) JOIN classifications USING (classification_id) WHERE classification_id = " + caId;

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Races race = new Races();
                race.setRaceId(resultSet.getInt("race_id"));
                race.setRaceName(resultSet.getString("race_name"));

                Classifications classification = new Classifications();
                classification.setCaId(resultSet.getInt("classification_id"));
                classification.setCaName(resultSet.getString("classification_name"));
                race.setClassifications(classification);

                Countries country = new Countries();
                country.setCrid(resultSet.getInt("country_id"));
                country.setCrname(resultSet.getString("country_name"));
                race.setCountries(country);

                racesList.add(race);
            }
            return racesList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public List<Races> findRacesByCountry(String countryName) {
        List<Races> racesList = new ArrayList<>();
        String query = "SELECT * FROM races JOIN countries USING (country_id) JOIN classifications USING (classification_id) WHERE country_name = '" + countryName + "'";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Races race = new Races();
                race.setRaceId(resultSet.getInt("race_id"));
                race.setRaceName(resultSet.getString("race_name"));

                Classifications classification = new Classifications();
                classification.setCaId(resultSet.getInt("classification_id"));
                classification.setCaName(resultSet.getString("classification_name"));
                race.setClassifications(classification);

                Countries country = new Countries();
                country.setCrid(resultSet.getInt("country_id"));
                country.setCrname(resultSet.getString("country_name"));
                race.setCountries(country);

                racesList.add(race);
            }
            return racesList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public void addRace(Races race) {
        String query = "INSERT INTO races (race_name, country_id, classification_id) VALUES ('" +
                race.getRaceName() + "', " + race.getCountry_id() + ", " + race.getClassicifaction_id() + ")";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteRace(String raceName) {
        String query = "DELETE FROM races WHERE race_name = '" + raceName + "'";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
