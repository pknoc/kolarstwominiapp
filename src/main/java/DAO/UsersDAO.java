package DAO;


import connection.CustomConnection;
import connection.MySqlConnector;
import entity.Users;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;

@Log4j
public class UsersDAO {

    private CustomConnection connector;

    public UsersDAO(){
        connector = new MySqlConnector();
    }

    public Users findByLogin (String login) {

        String query = "select * from users where user_login='" + login + "';";

        try (Connection connection = connector.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            if (resultSet.next()) {
                Users user = new Users();
                user.setUserId(resultSet.getInt("user_id"));
                user.setUserpassword(resultSet.getString("user_password"));
                user.setIsAdmin(resultSet.getInt("is_admin"));
                user.setLogin(resultSet.getString("user_login"));
                return user;
            }
            }catch(Exception e){
                log.error(e.getMessage());
            }
            return null;
        }
    public Users showUserData (String login) {

        String query = "select * from users where user_login='" + login + "'";

        try (Connection connection = connector.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query);) {

            if(resultSet.next()) {
                Users user = new Users();
                user.setUserId(resultSet.getInt("user_id"));
                user.setLogin(resultSet.getString("user_login"));
                user.setUserpassword(resultSet.getString("user_password"));
                user.setIsAdmin(resultSet.getInt("is_admin"));

                return user;
            }
        }catch(Exception e){
            log.error(e.getMessage());
        }
        return null;
    }

    }

