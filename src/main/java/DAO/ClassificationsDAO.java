package DAO;

import connection.CustomConnection;
import connection.MySqlConnector;
import entity.Classifications;
import entity.Countries;
import entity.Riders;
import entity.Teams;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClassificationsDAO {

    private CustomConnection connector;

    public ClassificationsDAO() {
        connector = new MySqlConnector();
    }

    public List<Classifications> findAllClassifications() {
        List<Classifications> classificationsList = new ArrayList<>();
        String query = "SELECT * FROM classifications";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Classifications classifications = new Classifications();
                classifications.setCaId(resultSet.getInt("classification_id"));
                classifications.setCaName(resultSet.getString("classification_name"));

                classificationsList.add(classifications);
            }
            return classificationsList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }
}
