package DAO;

import connection.CustomConnection;
import connection.MySqlConnector;
import entity.Countries;
import entity.Riders;
import entity.Teams;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RidersDAO {

    private CustomConnection connector;

    public RidersDAO() {
        connector = new MySqlConnector();
    }

    public List<Riders> findAllRiders() {
        List<Riders> ridersList = new ArrayList<>();
        String query = "SELECT * FROM riders JOIN countries USING (country_id) JOIN teams USING (team_id)";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Riders rider = new Riders();
                rider.setRiderId(resultSet.getInt("rider_id"));
                rider.setName(resultSet.getString("name"));
                rider.setSurname(resultSet.getString("surname"));

                Teams team = new Teams();
                team.setTeamId(resultSet.getInt("team_id"));
                team.setTeamName(resultSet.getString("team_name"));
                Countries countries = new Countries();
                countries.setCrid(resultSet.getInt("country_id"));
                countries.setCrname(resultSet.getString("country_name"));
                team.setCountries(countries);
                rider.setTeams(team);

                Countries country = new Countries();
                country.setCrid(resultSet.getInt("country_id"));
                country.setCrname(resultSet.getString("country_name"));
                rider.setCountries(country);

                ridersList.add(rider);
            }
            return ridersList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public List<Riders> findRidersByTeam(int teamId) {
        List<Riders> ridersList = new ArrayList<>();
        String query = "SELECT * FROM riders JOIN countries USING (country_id) JOIN teams USING (team_id) WHERE team_id = " + teamId;

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Riders rider = new Riders();
                rider.setRiderId(resultSet.getInt("rider_id"));
                rider.setName(resultSet.getString("name"));
                rider.setSurname(resultSet.getString("surname"));

                Teams team = new Teams();
                team.setTeamId(resultSet.getInt("team_id"));
                team.setTeamName(resultSet.getString("team_name"));
                Countries countries = new Countries();
                countries.setCrid(resultSet.getInt("country_id"));
                countries.setCrname(resultSet.getString("country_name"));
                team.setCountries(countries);
                rider.setTeams(team);

                Countries country = new Countries();
                country.setCrid(resultSet.getInt("country_id"));
                country.setCrname(resultSet.getString("country_name"));
                rider.setCountries(country);

                ridersList.add(rider);
            }
            return ridersList;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public Riders findRidersBySurname(String lastName) {
        String query = "SELECT * FROM riders JOIN countries USING (country_id) JOIN teams USING (team_id) WHERE surname ='" + lastName + "'";

        Riders rider = new Riders();
        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                rider.setRiderId(resultSet.getInt("rider_id"));
                rider.setName(resultSet.getString("name"));
                rider.setSurname(resultSet.getString("surname"));

                Teams team = new Teams();
                team.setTeamId(resultSet.getInt("team_id"));
                team.setTeamName(resultSet.getString("team_name"));
                Countries countries = new Countries();
                countries.setCrid(resultSet.getInt("country_id"));
                countries.setCrname(resultSet.getString("country_name"));
                team.setCountries(countries);
                rider.setTeams(team);

                Countries country = new Countries();
                country.setCrid(resultSet.getInt("country_id"));
                country.setCrname(resultSet.getString("country_name"));
                rider.setCountries(country);
            }

            return rider;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }

    public void addRider(Riders rider) {
        String query = "INSERT INTO riders (name, surname, team_id, country_id) VALUES ('" +
                rider.getName() + "', '" + rider.getSurname() + "', " + rider.getTeam_id() + ", " + rider.getCountry_id() + ")";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void deleteRider(String surname) {
        String query = "DELETE FROM riders WHERE surname = '" + surname + "'";

        try (Connection connection = connector.getConnection()) {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
