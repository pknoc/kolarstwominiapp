package connection;

import java.sql.Connection;

public interface CustomConnection {
    Connection getConnection();
}
