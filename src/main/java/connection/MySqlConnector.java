package connection;

import properties.PropertiesReader;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.*;

public class MySqlConnector implements CustomConnection {
    private static final Logger logger = Logger.getLogger(MySqlConnector.class);

    private Properties properties;

    public MySqlConnector() {

    }
    @Override
    public Connection getConnection() {
        Connection con;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(
                    "jdbc:mysql://db4free.net:3306/cyclingapp","jakolborski","rootroot");

            con.setAutoCommit(true);
            return con;

        } catch (Exception e) {
            logger.error("Connection Failed! Check output console");
            logger.error(e);
        }

        return null;
    }



}
