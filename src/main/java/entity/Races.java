package entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class Races {

    private int raceId;
    private  String raceName;
    private int classicifaction_id;
    private Classifications classifications;
    private int country_id;
    private Countries countries;
//    private int startId;
//    private int finishId;
}
