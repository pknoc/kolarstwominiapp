package entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class Users {
    private int userId;
    private String login;
    private String userpassword;
    private int isAdmin;
}
