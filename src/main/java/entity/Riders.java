package entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class Riders {
    private int riderId;
    private String name;
    private String surname;
    private int team_id;
    private Teams teams;
    private int country_id;
    private Countries countries;

}
