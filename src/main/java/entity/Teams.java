package entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class Teams {
    private int teamId;
    private String teamName;
    private Countries countries;
}
