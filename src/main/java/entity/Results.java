package entity;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

public class Results implements Comparable<Results>{
    private int place_id;
    private Places place;
    private int rider_id;
    private Riders riders;
    private int race_id;
    private Races races;

    @Override
    public int compareTo(Results o) {
        int result = Integer.compare(this.getPlace().getPlaceid(), o.getPlace().getPlaceid());
        return result;
    }

}
