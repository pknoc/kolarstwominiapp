package menu;

import DAO.*;
import entity.*;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MenuFuncts {

    ResultsDAO resultsDAO = new ResultsDAO();
    RacesDAO racesDAO = new RacesDAO();
    ClassificationsDAO classificationsDAO = new ClassificationsDAO();
    Riders riders = new Riders();
    RidersDAO ridersDAO = new RidersDAO();
    Teams teams = new Teams();
    TeamsDAO teamsDAO = new TeamsDAO();

    public Integer startMenu() {
        UsersDAO usersDAO = new UsersDAO();
        Scanner scan = new Scanner(System.in);
        String x, login, password;
        Users user = new Users();
        boolean menu1 = true;

        // MENU POCZATKOWE

        System.out.println("Witamy w Kolarskim mini Wiki");
        while (menu1) {
            System.out.println("Wybierz co chcesz zrobić\n(Wprowadz liczbe):\n1. Zaloguj sie.\n2. Zakoncz program");
            x = scan.nextLine();

            if (x.equals("2")) {
                System.out.println("Zakonczenie programu");
                System.exit(0);
                // menu1 = false;
            } else if (x.equals("1")) {
                System.out.println("Podaj login:");
                try {
                    login = scan.nextLine();
                    user = usersDAO.findByLogin(login);
                    System.out.println("Podaj haslo:");
                    password = scan.nextLine();
                    if (password.equals(user.getUserpassword())) {
                        System.out.println("Logowanie pomyslne");
                        menu1 = false;
                        return user.getIsAdmin();
                    } else {
                        System.out.println("Zly login lub haslo. \n Sprobuj jeszcze raz");
                    }
                } catch (NullPointerException e) {
                    System.out.println("Zly login lub haslo. \n Sprobuj jeszcze raz");
                }
            } else {
                System.out.println("Bledna komenda.\nSprobuj jeszcze raz.");
            }
        }
        return null;
    }

    public void userMenu(int i) {


        Scanner scan = new Scanner(System.in);
        String x;

        boolean menuFlag = true;
        if (i == 0) {
            System.out.println("Witamy w menu uzytkowniaka.");
        } else {
            System.out.println("Menu uzytkownika:");
        }
        while (menuFlag) {

            System.out.println("Co chcesz zrobic?");
            System.out.println("1. Wyswietl liste zawodnikow." +
                    "\n2. Wyswietl dane zawodnika o nazwisku." +
                    "\n3. Wyswietl liste teamow." +
                    "\n4. Wyswietl liste zawodnikow teamu." +
                    "\n5. Wyswietl liste wyscigow." +
                    "\n6. Wyswietl podium danego wyscigu." +
                    "\n7. Wyswietl liste wyscigow ze wzgledu na kraj." +
                    "\n8. Wyswietl liste wyscigow ze wzgledu na klase.");
            if (i == 0) {
                System.out.println("9. Zakoncz program.");
            } else {
                System.out.println("9. Wroc do panelu admnistratora.");
            }
            x = scan.nextLine();
            switch (x) {
                case "1":
                    List<Riders> ridersList = ridersDAO.findAllRiders();
                    for (Riders r : ridersList) {
                        System.out.println("Rider ID: " + r.getRiderId() + " Name: " + r.getName() + " " + r.getSurname());
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "2":
                    System.out.println("Podaj nazwisko zawodnika:");
                    String rider = scan.nextLine();
                    riders = ridersDAO.findRidersBySurname(rider);
                    try {
                        if (riders.getSurname().equals(rider)) {

                            System.out.println("ID Zawodnika: " + riders.getRiderId() + " \nImie i nazwisko: " + riders.getName() + " " + riders.getSurname() + " \nTeam: " + riders.getTeams().getTeamName() + " \nKraj: " + riders.getCountries().getCrname());
                        }
                    } catch (NullPointerException e) {
                        e.getMessage();
                        System.out.println("Brak zawodnika w bazie");
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "3":
                    List<Teams> teamsList = teamsDAO.findAllTeams();
                    for (Teams t : teamsList) {
                        System.out.println("Team ID: " + t.getTeamId() + " Team name: " + t.getTeamName() + " Country: " + t.getCountries().getCrname());
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "4":
                    System.out.println("Podaj ID Teamu, ktory chcesz wyswietlic." +
                            "\n(Jesli nie znasz ID wpisz 'checkteams' aby sprawdzic liste).");
                    int teamChoice;
                    String wybor = scan.nextLine();
                    if (wybor.equals("checkteams")) {
                        List<Teams> teamsIdList = teamsDAO.findAllTeams();
                        for (Teams t : teamsIdList) {
                            System.out.println("Team ID: " + t.getTeamId() + " Team name: " + t.getTeamName());
                        }
                        System.out.println("Podaj ID:");
                        teamChoice = scan.nextInt();
                    } else {
                        try {
                            teamChoice = Integer.valueOf(wybor);
                        } catch (NumberFormatException e) {
                            e.getMessage();
                            System.out.println("Zle ID.");
                            teamChoice = 0;
                        }
                    }
                    List<Riders> ridersByTeam = ridersDAO.findRidersByTeam(teamChoice);
                    if (ridersByTeam.isEmpty()) {
                        System.out.println("Nie ma druzyny o takim ID");
                    } else {
                        for (Riders r : ridersByTeam) {
                            System.out.println("Rider ID: " + r.getRiderId() + " Rider full name: " + r.getName() + " " + r.getSurname() + " Country: " + r.getCountries().getCrname());
                        }
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "5":
                    List<Races> allRaces = racesDAO.findAllRaces();
                    System.out.println("Wyscigi znajdujace sie w bazie danych:");
                    for (Races rc : allRaces) {
                        System.out.println("ID wyscigu: " + rc.getRaceId() + " Nazwa wyscigu: " + rc.getRaceName() + " Kraj: " + rc.getCountries().getCrname());
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "6":
                    allRaces = racesDAO.findAllRaces();
                    System.out.println("Wyscigi znajdujace sie w bazie danych:");
                    for (Races rc : allRaces) {
                        System.out.println("ID wyscigu: " + rc.getRaceId() + " Nazwa wyscigu: " + rc.getRaceName() + " Kraj: " + rc.getCountries().getCrname());
                    }
                    System.out.println("Podaj id wyscigu: ");
                    try {
                        int rcId = Integer.valueOf(scan.nextLine());
                        List<Results> podiumList = resultsDAO.findResultByRace(rcId);
                        if (podiumList.isEmpty()) {
                            System.out.println("Brak wyscigu o danym ID.");
                        } else {
                            System.out.println("Podium wyscigu o ID: " + rcId + " to:");
                            for (Results rs : podiumList) {
                                System.out.println("Miejsce: " + rs.getPlace().getPlaceName() + " - " + rs.getRiders().getName() + " " + rs.getRiders().getSurname() + " | Team: " + rs.getRiders().getTeams().getTeamName());
                            }
                        }
                    } catch (InputMismatchException e) {
                        e.getMessage();
                        System.out.println("Bledny format ID");
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "7":
                    System.out.println("Podaj nazwe kraju aby sprawdzic liste dostepnych wyscigow" +
                            "\n(Nazwy wprowadz w jezyku angielskim)");
                    String countryName = scan.nextLine();
                    List<Races> racesByCountryList = racesDAO.findRacesByCountry(countryName);
                    if (racesByCountryList.isEmpty()) {
                        System.out.println("Brak wyscigow dla podanego kraju.");
                    } else {
                        for (Races rc : racesByCountryList) {
                            System.out.println("ID wyscigu: " + rc.getRaceId() + " | Nazwa: " + rc.getRaceName());
                        }
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "8":
                    System.out.println("Rodzaje wyscigow:");
                    List<Classifications> classificationsList = classificationsDAO.findAllClassifications();
                    for (Classifications clsft : classificationsList) {
                        System.out.println("ID rodzaju: " + clsft.getCaId() + " Nazwa: " + clsft.getCaName());
                    }
                    System.out.println("Podaj ID rodzaju ktory chcesz sprawdzic:");
                    try {
                        int clsChoice = Integer.valueOf(scan.nextLine());

                        List<Races> racesList = racesDAO.findRacesByClassification(clsChoice);
                        if (racesList.isEmpty()) {
                            System.out.println("Brak wyników dla podanego id");
                        } else {
                            for (Races r : racesList) {
                                System.out.println("ID Wyscigu: " + r.getRaceId() + " Nazwa wyscigu: " + r.getRaceName());
                            }
                        }
                    } catch (InputMismatchException e) {
                        e.getMessage();
                        System.out.println("Zle ID");
                    }
                    System.out.println("\nKliknij enter aby wrocic do menu.");
                    scan.nextLine();
                    break;
                case "9":
                    menuFlag = false;

                    break;
                default:
                    System.out.println("Brak opcji o wprowadzonym znaku." +
                            "\n Kliknij enter aby kontynuowac.");
                    scan.nextLine();
                    break;

            }
        }
    }

    public void adminMenu() {
        Scanner scan = new Scanner(System.in);


        System.out.println("Witamy w Panelu administratora.");
        boolean menuFlag = true;
        while (menuFlag) {
            System.out.println("Dostepne funkcje:");
            System.out.println("1. Edycja zawodnikow." +
                    "\n2. Edycja wyscigow." +
                    "\n3. Edytuj wyniki wyscigow" +
                    "\n4. Panel uzytkownika" +
                    "\n5. Zakoncz program.");

            String menuPick = scan.nextLine();

            switch (menuPick) {
                case "1":
                    System.out.println("Co chcesz zrobic?" +
                            "\n 1. Dodaj zawodnika" +
                            "\n 2. Usun zawodnika." +
                            "\n 3. Powrot.");

                    String minimenu = scan.nextLine();

                    switch (minimenu) {
                        case "1":
                            Riders newRider = new Riders();
                            System.out.println("Podaj imie zawodnika:");
                            newRider.setName(scan.nextLine());
                            System.out.println("Podaj nazwisko zawodnika:");
                            newRider.setSurname(scan.nextLine());
                            try {
                                System.out.println("Podaj ID Teamu do jakiego ma zostac przypisany zawodnik");
                                newRider.setTeam_id(Integer.valueOf(scan.nextLine()));
                                System.out.println("Podaj ID kraju do jakiego ma zostac przypisany zawodnik");
                                newRider.setCountry_id(Integer.valueOf(scan.nextLine()));
                                ridersDAO.addRider(newRider);
                                System.out.println("Zawodnik dodany." +
                                        "\nKliknij Enter aby wrocic do Panelu Administratora.");
                                scan.nextLine();
                            } catch (NumberFormatException e) {
                                e.getMessage();
                                System.out.println("Wprowadzony format danych jest bledny. " +
                                        "\nZawodnik nie zostanie dodany." +
                                        "\nKliknij Enter aby wrocic do Panelu Administratora.");
                                scan.nextLine();

                            }
                            break;
                        case "2":

                            System.out.println("Podaj nazwisko zawodnika, ktory ma zostac usuniety:");
                            String deleteRiderBySurname = scan.nextLine();

                            ridersDAO.deleteRider(deleteRiderBySurname);

                            System.out.println("Zawodnik " + deleteRiderBySurname + " zostal usuniety.");
                            System.out.println("Kliknij Enter aby wrocic do Panelu Administratora.");
                            scan.nextLine();

                            // USUWANIE

                            break;
                        case "3":
                            break;
                        default:
                            System.out.println("Brak wskazanej opcji." +
                                    "\n\n Kliknij Enter aby wrocic do Panelu Administratora.");
                            scan.nextLine();
                    }

                    break;

                case "2":
                    System.out.println("Co chcesz zrobic?" +
                            "\n 1. Dodaj wyscig." +
                            "\n 2. Usun wyscig." +
                            "\n 3. Powrot");
                    minimenu = scan.nextLine();
                    switch (minimenu) {
                        case "1":
                            Races newRace = new Races();
                            try {
                                System.out.println("Podaj nazwe wyscigu:");
                                newRace.setRaceName(scan.nextLine());
                                System.out.println("Podaj ID kraju w ktorym odbywa sie wyscig:");
                                newRace.setCountry_id(Integer.valueOf(scan.nextLine()));
                                System.out.println("Podaj ID klasy wyscigu:");
                                newRace.setClassicifaction_id(Integer.valueOf(scan.nextLine()));
                                racesDAO.addRace(newRace);
                                System.out.println("Wyscig dodany." +
                                        "\nKliknij Enter aby wrocic do Panelu Administratora.");
                                scan.nextLine();

                            } catch (InputMismatchException e) {
                                e.getMessage();
                                System.out.println("Wprowadzony format danych jest bledny." +
                                        "\nKliknij Enter aby wrocic do Panelu Administratora.");
                                scan.nextLine();
                            }

                            break;
                        case "2":
                            System.out.println("Podaj nazwe wyscigu, ktory ma zostac usuniety.");
                            String raceToDelete = scan.nextLine();
                            racesDAO.deleteRace(raceToDelete);
                            System.out.println("Wyscig " + raceToDelete + " zostal usuniety.");


                            break;
                        case "3":
                            break;
                        default:
                            System.out.println("Nie ma takiej opcji." +
                                    "\nKliknij Enter aby wrocic do Panelu Administratora.");
                            scan.nextLine();
                    }
                    break;

                case "3":
                    System.out.println("Co chcesz zrobic?" +
                            "\n 1. Dodaj wynik." +
                            "\n 2. Usun wynik." +
                            "\n 3. Powrot.");
                    minimenu = scan.nextLine();
                    switch (minimenu) {
                        case "1":
                            Results newResult = new Results();
                            try {
                                List<Riders> ridersList = ridersDAO.findAllRiders();
                                for (Riders r : ridersList) {
                                    System.out.println("Rider ID: " + r.getRiderId() + " Name: " + r.getName() + " " + r.getSurname());
                                }
                                System.out.println("Podaj ID zawodnika ktorego wynik chcesz wprowadzic:");
                                newResult.setRider_id(Integer.valueOf(scan.nextLine()));
                                List<Races> allRaces = racesDAO.findAllRaces();
                                System.out.println("Wyscigi znajdujace sie w bazie danych:");
                                for (Races rc : allRaces) {
                                    System.out.println("ID wyscigu: " + rc.getRaceId() + " Nazwa wyscigu: " + rc.getRaceName() + " Kraj: " + rc.getCountries().getCrname());
                                }
                                System.out.println("Podaj ID wyscigu dla ktorego ma zostac wprowadzony wynik:");
                                newResult.setRace_id(Integer.valueOf(scan.nextLine()));
                                System.out.println("Podaj pozycję zajetą przez zawodnika.");
                                newResult.setPlace_id(Integer.valueOf(scan.nextLine()));
                                resultsDAO.addResults(newResult);
                            } catch (InputMismatchException e) {
                                e.getMessage();
                                System.out.println("Bledny format daych." +
                                        "\nKliknij Enter aby wrocic do Panelu Administratora.");
                                scan.nextLine();
                            }
                            break;
                        case "2":
                            List<Races> allRaces = racesDAO.findAllRaces();
                            System.out.println("Wyscigi znajdujace sie w bazie danych:");
                            for (Races rc : allRaces) {
                                System.out.println("ID wyscigu: " + rc.getRaceId() + " Nazwa wyscigu: " + rc.getRaceName() + " Kraj: " + rc.getCountries().getCrname());
                            }
                            System.out.println("Podaj ID wyscigu dla ktorego chcesz usunac wyniki:");
                            try {
                                resultsDAO.deleteResults(Integer.valueOf(scan.nextLine()));
                            } catch (InputMismatchException e) {
                                e.getMessage();
                                System.out.println("Bledny format ID." +
                                        "\nKliknij Enter aby wrocic do Panelu Administratora.");
                                scan.nextLine();
                            }
                            break;
                        case "3":
                            break;
                        default:
                            System.out.println("Nie ma takiej opcji." +
                                    "\nKliknij Enter aby wrocic do Panelu Administratora.");
                            scan.nextLine();
                            break;
                    }

                    break;
                case "4":
                    MenuFuncts menuUser = new MenuFuncts();
                    menuUser.userMenu(1);
                    break;
                case "5":

                    System.out.println("Zakonczenie programu.");
                    System.exit(0);

                    break;
                default:

                    System.out.println("Nie ma takiego wyboru." +
                            "\nKliknij Enter aby wrocic do Panelu Administratora.");
                    scan.nextLine();
                    break;
            }

        }
    }
}
